/*globals console, _*/

_.templateSettings = {
    interpolate: /\{\{\=(.+?)\}\}/g,
    escape: /\{\{\-(.+?)\}\}/g,
    evaluate: /\{\{(.+?)\}\}/g
};

var Biglaker = {};

Biglaker.factory = (function($, src) {
    'use strict';
    var $cache,
        que = {},
        template = {},
        loading = false,
    
        cleanUp = function (elem) {
            if (que[elem].counter > 1) {
                que[elem].counter-=1;
            } else {
                que[elem] = null; 
                delete que[elem];
            }
        },
        
        getFromCache = function (elem) {
            if (template[elem] === undefined) {
                template[elem] = _.template($cache.find(elem).html());        
            }
            que[elem].deferred.resolve(template[elem]);
            if (que[elem].deferred.state() === 'resolved') {
                cleanUp(elem);
            }
        },
        
        setupQue = function (elem) {           
            if (que[elem] === undefined) {
                que[elem] = {
                    deferred : $.Deferred(),
                    counter : 1
                };
            } else {
                que[elem].counter+=1;
            }           
        },
        
        loadTemplate = function (elem) {   
            loading = true;                   
            var promise = $.ajax({ 
                url: src,
                dataType : 'html' 
            });
            return promise;
        },
        /*
         * only the first request will trigger loadTemplate
         * if getTemplate is called recursively the second parameter as boolean will be passed
         */
        getTemplate = function (elem, waiting) {
            // execute setup when not waiting for ajax return
            if (!waiting) {
                setupQue(elem);
            }
            // load template
            if ($cache === undefined && !loading) { 
                loadTemplate(elem).done(function (data) {
                    $cache = $('<div></div>').html(data);
                    getFromCache(elem);
                });
            } else { 
                if ($cache !== undefined) {
                    // cache ready
                    getFromCache(elem);   
                } else {
                    // cache not ready, delay recursive call
                    setTimeout(function() {
                        getTemplate(elem, true);
                    }, 100);
                }
            }
            // if waiting for ajax, don't return empty promise
            if (waiting) {
                return false;
            } 
            return que[elem].deferred.promise();
        };

    return {
        getTemplate: getTemplate
    };
}(jQuery, 'data/template.html'));

Biglaker.HeaderView = Backbone.View.extend({
    initialize : function (options) {
        'use strict';
        this.label = options.label;
        this.render();
    },
      
    render : function () {
        'use strict';
        var that = this,
            id = '#tpl-header';
        Biglaker.factory.getTemplate(id).done(function (template) {
            that.$el.html(template({label: that.label}));
        }); 
        return this;
    }   
});

Biglaker.BodyView = Backbone.View.extend({
    label : '',
    initialize : function (options) {
        'use strict';
        this.label = options.label;
        this.render();
    },
      
    render : function () {
        'use strict';
        var that = this,
            id = '#tpl-body';
        Biglaker.factory.getTemplate(id).done(function (template) {
            that.$el.html(template({label: that.label}));
        }); 
        return this;
    }   
});


Biglaker.app = (function($) {
    'use strict';
    var slides,
        data,
        headerView1,
        headerView2,
        bodyView1,
        bodyView2,

        initialize = function () {
            headerView1 = new Biglaker.HeaderView({
                el: '#header1',
                label: 'Header View 1'
            });
            headerView2 = new Biglaker.HeaderView({
                el: '#header2',
                label: 'Header View 2'
            });
            bodyView1= new Biglaker.BodyView({
                el: '#body1',
                label: 'Body View 1'
            });
            bodyView2 = new Biglaker.BodyView({
                el: '#body2',
                label: 'Body View 2'
            });
        };

    return {
        initialize: initialize
    };
}(jQuery));


jQuery(function() {
    'use strict';
    Biglaker.app.initialize();
});
